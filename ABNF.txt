
programa = 	"ingredientes"  declaracion_variables "elaboracion" sentencias /*Poner final?*/

 /*  Declaracion de variables */
declaracion_variables =    [declaracion_variables declaracion_variable "."]
declaracion_variable =  declaracion_int / declaracion_string

declaracion_int =  numero   "g"   variable_id
declaracion_string =  numero   "ml"   variable_id   "marca"   string 

 /*  Sentencias */
sentencias = [ sentencias sentencia ] 
sentencia = 	sentencia_if /	sentencia_asignacion /	sentencia_while

sentencia_asignacion = 	tipo_asignacion "."
sentencia_if = 	"si" expresion "luego" sentenciasBloque "fin"
sentencia_while =  "mientras"  expresion "repetir" sentenciasBloque "fin"


/* Asignaciones*/
tipo_asignacion  = 	asignacion_normal / asignacion_agregacion / asignacion_resta / asignacion_multiplicacion / asignacion_division / asignacion_modulo / asignacion_string

asignacion_normal =  "poner" expresion_matematica "en"  variable_id
asignacion_string =  "poner" string "en" variable_id
asignacion_agregacion =  "agregar"  expresion_matematica ("a"/"al") variable_id 
asignacion_resta =  "filtrar"  expresion_matematica "de" variable_id 
asignacion_multiplicacion =  "batir"  variable_id TOKEN_CON expresion_matematica
asignacion_division =  "cortar"  variable_id "en" expresion_matematica 
asignacion_modulo =  "sazonar"  variable_id TOKEN_CON expresion_matematica

expresion =  ("no" expresion) /  expresionO
expresionO =  (expresionO ("o"/"u") expresionY) /  expresionY
expresionY =  ( expresionY  ("y"/"e") expresion_relacional ) / 	expresion_relacional
expresion_relacional =  ( expresion_relacional  ("<"/">"/">="/"<="/"=="/"!=") operando) /  operando
expresion_matematica = variableID / NUMERO

operando =  ( "("  expresion ")" ) / variable_id /  numero
variable_id = ALPHA *(ALPHA/"_")
string = 1*(DIGIT/ALPHA)
numero = 1*DIGIT