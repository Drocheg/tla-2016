



%{
#define FUNCTION_AUX _f
#define STRING_CONST 1
#define INT_CONST 2
#define ERROR_CODE -42
#include <stdio.h>
#include <stdlib.h>
#include "tempWriter.h"
#include "tlaMapImpl.h"
extern int yylineno;
int counterFunction = 1; 
int getNewFunctionIndex();
void writeAsignation(char * variableName,int functionCounter,char* operand, int thisFunctionCounter);
void yyerror(char* msg){
	printf("Error de syntaxis cerca de la linea %d\n",yylineno);
}
%}

%union
{
        int numero;
        char* string;
}

%token TOKEN_MOSTRAR TOKEN_PUNTO TOKEN_CON TOKEN_G TOKEN_ML TOKEN_MARCA TOKEN_SI TOKEN_SI_AUX TOKEN_FIN TOKEN_PONER TOKEN_EN TOKEN_AGREGAR TOKEN_AGREGAR_AUX  TOKEN_MULTIPLICACION TOKEN_MULTIPLICACION_AUX TOKEN_DIVISION TOKEN_DIVISION_AUX TOKEN_MODULO TOKEN_MODULO_AUX TOKEN_MIENTRAS TOKEN_REPETIR TOKEN_NO TOKEN_RESTA TOKEN_RESTA_AUX TOKEN_PARENTSIS_APERTURA TOKEN_PARENTESIS_CIERRE TOKEN_INICIO_VARIABLES TOKEN_FIN_VARIABLES

%token <string> NUMERO
%token <string> VARIABLE_ID
%token <string> STRING
%token <string> TOKEN_RELACION
%token <string> TOKEN_Y
%token <string> TOKEN_O

%type <numero> declaracion_variable declaracion_string declaracion_int sentencia sentencia_if sentencia_asignacion sentencia_while
%type <numero> tipo_print sentencia_print sentenciasBloque tipo_asignacion asignacion_normal asignacion_agregacion asignacion_resta asignacion_multiplicacion asignacion_division asignacion_modulo asignacion_string expresion expresionO expresionY expresion_relacional operando expresion_matematica


%start programa
%% 


programa:	TOKEN_INICIO_VARIABLES  declaracion_variables TOKEN_FIN_VARIABLES
{
	writeToMain("main(int argc, char *argv[]){\n");
}sentenciasMain
{
	writeToMain("\n}\n");
}
;
 /*  Declaracion de variables */
declaracion_variables: /*  blank  */
|	declaracion_variables declaracion_variable TOKEN_PUNTO
;
declaracion_variable: declaracion_int
|	declaracion_string
;


declaracion_int: NUMERO   TOKEN_G   VARIABLE_ID
{
        if(!containsKeyMap($3)){
        	int type = INT_CONST;
        	writeToFunctions("int %s = %s;\n",$3,$1);
        	putMap($3,type);
        }else{
        	printf("Linea: %d Error: declaracion previa del ingrediente %s\n", yylineno ,$3);
        	exit(ERROR_CODE);
        }
		
}
;

declaracion_string: NUMERO   TOKEN_ML   VARIABLE_ID   TOKEN_MARCA   STRING 
{
        if(!containsKeyMap($3)){
        	int type = STRING_CONST;
        	writeToFunctions("char* %s = %s;\n",$3,$5);
        	putMap($3,type);
        }else{
        	printf("Linea: %d Error: declaracion previa del ingrediente %s\n",yylineno ,$3);
        	exit(ERROR_CODE);
        }
        
}
;


 /*  Sentencias */
sentenciasMain:	 /*  blank  */
|sentenciasMain sentencia 
{
	writeToMain("_f%d();\n",$2);
}
;

sentenciasBloque:	 /*  blank  */{
	$$ = 0;
}
|sentenciasBloque sentencia {

	int index = getNewFunctionIndex();
	$$ = index;
	writeToFunctions("void _f%d(){ _f%d(); _f%d(); }\n",index, $1, $2);
}
;


sentencia:	sentencia_if
{
	//writeToMain("_f%d();\n",$1);
	$$ = $1;
}
;
	|	sentencia_asignacion
	{
	//writeToMain("_f%d();\n",$1);
	$$ = $1;
}
;
	|	sentencia_while
	{
	//writeToMain("_f%d();\n",$1);
	$$ = $1;
}
;
	|	sentencia_print
{
	//writeToMain("_f%d();\n",$1);
	$$ = $1;
}
;
sentencia_if:	TOKEN_SI  expresion TOKEN_SI_AUX sentenciasBloque TOKEN_FIN
{		
int index = getNewFunctionIndex();	
		$$ = index;
		writeToFunctions("void _f%d(){ if (  _f%d() ) { _f%d(); } }\n",index, $2, $4);
}
;

sentencia_print: TOKEN_MOSTRAR tipo_print TOKEN_PUNTO
{
	$$ = $2;
} ;

tipo_print: VARIABLE_ID  {		
		int index = getNewFunctionIndex();
		$$ = index;
		int type;
		int exists = getValueMap($1,&type);
		if(exists == 0){
			if(type == INT_CONST){
				writeToFunctions("void _f%d(){ printf(\"%%d\\n\",%s); }\n",index, $1);
			}else if(type == STRING_CONST){
				writeToFunctions("void _f%d(){ printf(\"%%s\\n\",%s); }\n",index, $1);
			}
		}else{
			printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$1);
			exit(ERROR_CODE);
		}
};
		| NUMERO {		
		int index = getNewFunctionIndex();
		$$ = index;
		writeToFunctions("void _f%d(){ printf(\"%s\\n\"); }\n",index, $1);
};
		| STRING  {		
		int index = getNewFunctionIndex();
		$$ = index;
		writeToFunctions("void _f%d(){ printf(%s);printf(\"\\n\");}\n",index, $1);
};

sentencia_asignacion:	tipo_asignacion TOKEN_PUNTO
{
	$$ = $1;
}
;








tipo_asignacion :	asignacion_normal  
| asignacion_agregacion 
| asignacion_resta
| asignacion_multiplicacion 
| asignacion_division 
| asignacion_modulo
| asignacion_string
{
            	$$ = $1;
       	}
       	;






asignacion_normal: TOKEN_PONER  expresion_matematica TOKEN_EN  VARIABLE_ID
{
	int type;
	int exists = getValueMap($4,&type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$4);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($4,$2," ",index);
			$$ = index;			
	}else{
		printf("Linea: %d Error: Se le esta poniendo un int al ingrediente %s, que es un String", yylineno ,$4);
		exit(ERROR_CODE);
	}


} 
;
asignacion_string: TOKEN_PONER STRING TOKEN_EN VARIABLE_ID
{
	int type;
	int exists = getValueMap($4,&type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$4);
		exit(ERROR_CODE);
	}
	if(type == STRING_CONST){
			int index = getNewFunctionIndex();
			writeToFunctions("void _f%d(){%s = %s;}\n" ,index,$4,$2);
			$$ = index;
	}else{
		printf("Linea: %d Error: Se le esta poniendo un String al ingrediente %s, que es un int", yylineno ,$4);
		exit(ERROR_CODE);
	}
}
;
asignacion_agregacion: TOKEN_AGREGAR  expresion_matematica TOKEN_AGREGAR_AUX VARIABLE_ID 
{
	int type;
	int exists = getValueMap($4, &type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$4);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($4,$2,"+",index);
			$$ = index;	
		
	}else{
		printf("Linea: %d Error: Se esta intentando agregar %s, que es un String", yylineno ,$4);
		exit(ERROR_CODE);
	}
}
;
asignacion_resta: TOKEN_RESTA  expresion_matematica TOKEN_RESTA_AUX VARIABLE_ID 
{

	int type;
	int exists = getValueMap($4, &type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$4);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($4,$2,"-",index);
			$$ = index;	
		
	}else{
		printf("Linea: %d Error: Se esta intentando filtrar %s, que es un String", yylineno ,$4);
		exit(ERROR_CODE);
	}
}
;

asignacion_multiplicacion: TOKEN_MULTIPLICACION  VARIABLE_ID TOKEN_CON expresion_matematica
{
	int type;
	int exists = getValueMap($2, &type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$2);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($2,$4,"*",index);
			$$ = index;	
		
	}else{
		printf("Linea: %d Error: Se esta intentando batir %s, que es un String", yylineno ,$2);
		exit(ERROR_CODE);
	}
}
;
asignacion_division: TOKEN_DIVISION  VARIABLE_ID TOKEN_EN expresion_matematica 
{
	int type;
	int exists = getValueMap($2, &type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$2);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($2,$4,"/",index);
			$$ = index;
		
	}else{
		printf("Linea: %d Error: Se esta intentando cortar %s, que es un String", yylineno ,$2);
		exit(ERROR_CODE);
	}
}
;
asignacion_modulo: TOKEN_MODULO  VARIABLE_ID TOKEN_CON expresion_matematica 
{
	int type;
	int exists = getValueMap($2, &type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno ,$2);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			writeAsignation($2,$4," %",index);
			$$ = index;	
		
	}else{
		printf("Linea: %d Error: Se esta intentando sazonar %s, que es un String", yylineno ,$2);
		exit(ERROR_CODE);
	}
}
;

sentencia_while: TOKEN_MIENTRAS  expresion TOKEN_REPETIR sentenciasBloque TOKEN_FIN
{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("void _f%d(){ while (  _f%d() ) { _f%d(); } }\n",index, $2, $4);		
}
;


expresion: TOKEN_NO expresion {
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return !_f%d();}\n",index, $2);
}
| expresionO
;

expresionO: expresionO TOKEN_O expresionY 
{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return _f%d() || _f%d();}\n",index, $1, $3);
}
| expresionY
;
expresionY: expresionY  TOKEN_Y expresion_relacional 
{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return _f%d() && _f%d();}\n",index, $1, $3);


}
|	expresion_relacional
;
expresion_relacional: expresion_relacional  TOKEN_RELACION operando
{
int index = getNewFunctionIndex();
$$ = index;
writeToFunctions("int _f%d(){ return _f%d() %s _f%d();}\n",index, $1,$2, $3);
} /* ; (??)*/ 
|	operando
;

expresion_matematica: VARIABLE_ID
{
	int type;
	int exists = getValueMap($1,&type);
	if(exists != 0){
		printf("Linea: %d Error: El ingrediente %s no fue declarado\n", yylineno,$1);
		exit(ERROR_CODE);
	}
	if(type == INT_CONST){
			int index = getNewFunctionIndex();
			$$ = index;	
			writeToFunctions("int _f%d(){ return %s;}\n",index, $1);
	}else{
		printf("Linea: %d Error: El ingrediente %s no es un int\n", yylineno ,$1);
		exit(ERROR_CODE);
	}
}
| NUMERO{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return %s;}\n",index, $1);
}
;








operando: TOKEN_PARENTSIS_APERTURA  expresion TOKEN_PARENTESIS_CIERRE
{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return _f%d();}\n",index, $2);
}

|VARIABLE_ID {
int type;
int exists = getValueMap($1,&type);
if(exists == 0){
	int index = getNewFunctionIndex();
	$$ = index;	
	writeToFunctions("int _f%d(){ return %s;}\n",index, $1);	
}else{
	printf("El ingrediente %s no fue declarado\n", $1 );
	exit(ERROR_CODE);
}
}

| NUMERO{
int index = getNewFunctionIndex();
$$ = index;	
writeToFunctions("int _f%d(){ return %s;}\n",index, $1);
}
;


 %%



main() {
	openFunctions();
	openMain();
    writeToFunctions("void _f0(){  }\n");
    
    yyparse();
} 




int getNewFunctionIndex(){
	return counterFunction++;
}




void writeAsignation(char * variableName,int functionCounter,char* operand, int thisFunctionCounter){


	writeToFunctions("void _f%d(){%s %s=_f%d();}\n" ,thisFunctionCounter,variableName,operand,functionCounter);
}






