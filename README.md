##Dependencias

- Flex
- YACC
- GCC
- Cygwin para Windows (con Flex, YACC y GCC)
- tempWriter.c y tempWriter.h (incluidos)

##Compilador

Para compilar el compilador de Recipe, ejecutar `make <platform>`, donde `<platform>` es `windows`, `osx` o `linux` según la máquina donde se esté corriendo.

Esto produce el `recipeCompiler` ejecutable (con extension `.exe` en Windows).

**NOTA:** La compilación del compilador puede generar warnings. Si el compilador se compila correctamente se mostrará un mensaje al terminar.

##Programas

1. Ejecutar el compilador de Recipe pasándole la receta por STDIN o redirigiendo la entrada a un archivo.
2. El compilador genera dos archivos intermedios, `recipeFunctions.c` y `recipeMain.c`
3. Si no hubo errores de compilación, para generar el ejecutable final correr `gcc recipeMain.c -o <output>` (no hace falta incluir recipeFunctions.c, el main ya se encarga de incluirlo)
4. Ejecutar el binario `<output>`
