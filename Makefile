.PHONY: windows linux osx lex yacc clean 

windows: lex yacc
	gcc tempWriter.c lex.yy.c y.tab.c hashmap.c tlaMapImpl.c -lfl -ly -o recipeCompiler.exe
	@echo "Recipe compiler created for Windows"

linux: lex yacc
	gcc tempWriter.c  lex.yy.c y.tab.c hashmap.c tlaMapImpl.c -lfl -ly -o recipeCompiler
	@echo "Recipe compiler created for Linux"

osx: lex yacc
	gcc tempWriter.c  lex.yy.c y.tab.c hashmap.c tlaMapImpl.c -ll -ly -o recipeCompiler
	@echo "Recipe compiler created for OSX"

lex:
	flex recipe.l

yacc:
	yacc -d recipe.y

clean:
	rm *.out *.exe
	rm recipeCompiler*
	rm recipeFunctions*
	rm recipeMain*