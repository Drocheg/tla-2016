%{
	#include <stdio.h>
	#include <string.h>
	#include "y.tab.h"
%}
   
%s	ELABORACION
%option yylineno

%%

"//".*\n					/* ignore comments */;
"/*"(.*|\n)*"*/"				/* ignore comments */;
\n                      /* ignore end of line */;
[ \t]+                  /* ignore whitespace */;
[0-9]+		{ yylval.string=strdup(yytext); return NUMERO; }
"g"		return TOKEN_G;
"ml" 		return TOKEN_ML;
"marca"	return TOKEN_MARCA;
"."		return TOKEN_PUNTO; 
"ingredientes"  return TOKEN_INICIO_VARIABLES;
"elaboracion"	{BEGIN(ELABORACION); return TOKEN_FIN_VARIABLES;}
"agregar"	return TOKEN_AGREGAR;
"a"|"al"		return TOKEN_AGREGAR_AUX;
"batir"		return TOKEN_MULTIPLICACION;
"filtrar"		return TOKEN_RESTA ;
"de"		return TOKEN_RESTA_AUX;
"cortar"		return TOKEN_DIVISION; 
"en"		return TOKEN_EN;
"mostrar"	return TOKEN_MOSTRAR;
"sazonar"	return TOKEN_MODULO;
"con" 		return TOKEN_CON;
"poner"		return TOKEN_PONER;
"si"		return TOKEN_SI;
"entonces"	return TOKEN_SI_AUX;
"fin"		return TOKEN_FIN;
"mientras"	return TOKEN_MIENTRAS;
"repetir"	return TOKEN_REPETIR;
"("		return TOKEN_PARENTSIS_APERTURA;
")"		return TOKEN_PARENTESIS_CIERRE;
"=="	|
"!="	|
"<"	|
">"	|
"<="	|
">="		{ yylval.string=strdup(yytext); return TOKEN_RELACION; }
"y"	|
"e"		{ yylval.string=strdup(yytext); return TOKEN_Y; }
"o"	|
"u"		{ yylval.string=strdup(yytext); return TOKEN_O; }
\".*\"		{ yylval.string=strdup(yytext); return STRING; }
"no"		return TOKEN_NO;
<ELABORACION>[a-zA-Z][a-zA-Z0-9_]*  {yylval.string= strdup(yytext); return VARIABLE_ID;}
[a-zA-Z][a-zA-Z0-9_]*  	{yylval.string= strdup(yytext); return VARIABLE_ID;}
.|\n         { yyterminate(); };
%%




