#include <stdio.h>
#include <time.h>

int isPrime;

int main(int argc, char **argv) {
	time_t start = time(NULL), end;
	for(int i = 10000; i < 100000; i++) {
		isPrime = 1;
		for(long j = 2; j < i; j++) {
			if(i % j == 0) {
				isPrime = 0;
				break;
			}
		}
		printf("%i is %sprime\n", i, isPrime ? "" : "NOT ");
	}
	end = time(NULL);
	printf("Took %li seconds\n", (long)end-start);
}